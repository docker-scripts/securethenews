#!/bin/bash

cmd_start() {
    docker-compose up -d
}

cmd_stop() {
    docker-compose stop
}

cmd_restart() {
    docker-compose restart
}

cmd_shell() {
    docker-compose exec -u root django bash
}

cmd_remove() {
    docker-compose down
    ds revproxy rm
}

cmd_make() {
    # add a revproxy domain and get a ssl-cert
    ds revproxy add
    ds revproxy ssl-cert

    # create a custom docker-compose.yaml file
    cat <<EOF > customizations.yaml
networks:
  app:
  ds:
    external:
      name: $NETWORK

services:
  postgresql:
    restart: unless-stopped
    volumes:
      - ./securethenews:/django
      - ./:/host
    environment:
      POSTGRES_DB: $POSTGRES_DB
      POSTGRES_USER: $POSTGRES_USER
      POSTGRES_PASSWORD: $POSTGRES_PASSWORD
    networks:
      app:
        aliases:
          - $POSTGRES_DB

  django:
    container_name: $CONTAINER
    image: $IMAGE
    restart: unless-stopped
    build:
      context: ./securethenews
      dockerfile: docker/ProdDjangoDockerfile
    environment:
      DJANGO_ALLOWED_HOSTS: $DOMAIN
      DJANGO_DB_HOST: $POSTGRES_DB
      DJANGO_DB_PASSWORD: $POSTGRES_PASSWORD
      DJANGO_DB_USER: $POSTGRES_USER
      DJANGO_DB_NAME: $POSTGRES_DB
    volumes:
      - ./django-media:/django-media
      - ./django-logs:/django-logs
      - ./:/host
    networks:
      app:
      ds:
        aliases:
          - $DOMAIN

EOF
    
    yq merge --overwrite \
       securethenews/prod-docker-compose.yaml \
       customizations.yaml > docker-compose.yaml
    rm customizations.yaml
    
    yq d -i docker-compose.yaml services.postgresql.ports
    yq d -i docker-compose.yaml services.django.ports
    yq d -P -i docker-compose.yaml volumes

    # build the images of the services
    docker-compose build --pull
    
    # make some other configurations
    ds config
}
