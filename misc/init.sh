# run custom init steps

# install yq
if ! hash yq; then
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CC86BB64
    add-apt-repository -y ppa:rmescandon/yq
    apt update
    apt install yq -y
fi

# get the code of securethenews
git clone https://github.com/freedomofpress/securethenews
