cmd_sites_help() {
    cat <<EOF
    sites export <file.csv>
    sites import <file.csv>
EOF
}

cmd_sites() {
    local cmd=$1
    local file=$2
    [[ -z $file ]] && fail "Usage:\n$(cmd_sites_help)\n"
    
    case $cmd in
        export)
            docker-compose exec -u root postgresql \
                           psql -q -c "\copy (select name, domain from sites_site) to '/host/$file' with CSV" \
                           $POSTGRES_DB $POSTGRES_USER
	    sed -i $file -e '1 i Organization Name,Domain Name'
            ;;
        import)
	    ds manage loadsites /host/$file && ds manage scan
            ;;
        *)
            fail "Usage:\n$(cmd_sites_help)\n"
            ;;
    esac
}
