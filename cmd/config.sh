cmd_config() {
    # create a cron jon that rescans the sites once a week
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-scan-sites
# rescan the sites each week
0 0 * * 0  root  bash -l -c "ds @$dir manage scan &> /dev/null"
EOF

}
