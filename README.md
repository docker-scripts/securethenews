# securethenews

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull securethenews`

  - Create a directory for the container: `ds init securethenews @securethenews.example.org`

  - Personalize the settings:
    `cd /var/ds/discourse.example.org/ ; vim settings.sh`

  - Build and start the containers: `ds make ; ds start`

  - Open https://securethenews.example.org and continue the setup
